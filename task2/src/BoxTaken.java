package kasparoop;

import java.io.*;

public class BoxTaken extends RuntimeException
{
    private Coordinate coord;

    public BoxTaken(Coordinate coord)
    {
        this.coord = coord;
    }

    @Override
    public String toString() {
        return "Coordinate " + coord.toString() + " is occupied";
    }
}