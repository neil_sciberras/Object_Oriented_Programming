package kasparoop;

public class Command {

    private final PlayerType player;
    private final CommandType commandType;
    private final Coordinate source;
    private final Coordinate destination;

    public Command(final PlayerType player,
                    final CommandType commandType,
                    final Coordinate source,
                    final Coordinate destination) {

        this.player = player;
        this.commandType = commandType;
        this.source = source;
        this.destination = destination;

    }

    public PlayerType getPlayer() {
        return player;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public Coordinate getSource() {
        return source;
    }

    public Coordinate getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        
        //this is done, since the the user's board's row numbers start from 1 till 8.
        //but in the actual design, the board starts from 0 till 7. 
        //so to output the number that matches with the user's board, we need to 
        //incrmeent the row number at output.
        int sourceRow = source.row + 1;
        int destRow = destination.row + 1;

        return player + "," + commandType + "," + source.getColChar() + sourceRow + "," + destination.getColChar() + destRow;
    }
}