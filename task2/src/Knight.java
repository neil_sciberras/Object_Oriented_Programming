package kasparoop;

import java.lang.Math;

public class Knight extends ChessPiece
{
    public Knight(int id, Coordinate location, PlayerType owner)
    {
        super('N', id, location, owner); //invoking the constructor of ChessPiece
    }

    boolean validMove(ChessBoard board, Coordinate destination)
    {
        if(!outOfBounds(destination))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int destRow = destination.row;
            int destCol = destination.col;

            //These two integers hold the differnece between the column number and 
            //row number of the source and destination.
            int colDifference = Math.abs(destCol-sourceCol);
            int rowDifference = Math.abs(destRow-sourceRow);

            //When the column difference is 1, then the row difference should be 2, and 
            //vice versa. So, the differences should be opposite/alternating.
            if(colDifference == 1)
            {
                if(rowDifference == 2)
                    return true;
                else 
                    return false;

            }else if(colDifference == 2)
            {
                if(rowDifference == 1)
                    return true;
                else 
                    return false;

            }else
                return false;

        }else
            return false; 
    }

}