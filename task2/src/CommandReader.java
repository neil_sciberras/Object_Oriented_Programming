package kasparoop;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//import kasparoop.CommandType;
//import kasparoop.PlayerType;

public class CommandReader {

    //These are string constants that are common for all the instances that can exist of 
    //this class (since they're static).
    private static final String DELIMITER = ",";

    //This method belongs to the class and not to an instance. It uses no instance variables.
    //Reads all commands, and returns a list of commands.
    public static List<Command> readCommands(String pathString, List<String> exceptionList) throws IOException {

        ////Converts the string into a Path object that can be opened.
        Path filePath = Paths.get(pathString);

        //A list that holds the lines read from the file. 
        List<String> lines;

        //The lis that will be returned by this method. This will take any of the two values:
        //  -empty list           : error in reading the file.
        //  -parseCommands(lines) : no error in reading the file. 
        List<Command> returnList;

        try {
            //Reads all lines from the path.
            //May throw an exception when there is an error opening the file.
            lines = Files.readAllLines(filePath);

            //If no exception has been caught, hecne no rethrow, hence this line is reached, 
            //then we set the return list to the list returned by the method parseCOmmands. 
            returnList = parseCommands(lines, exceptionList);

        }catch(IOException e)
        {
            //Catching errors from readAllLines()

            //If we cannot read the file, we return an empty list. 
            returnList = Collections.emptyList();

            //re-throwing the same exception, so it is handled in the launcher. 
            throw e;
        }

        return returnList;
    }

    //THis is a private static method, meaning that it cannot be invoked from outer classes.
    //This can just be used from this class. 
    private static List<Command> parseCommands(List<String> lines, List<String> exceptionList) 
    {

        //This will hold the parsed commands.
        List<Command> commands = new ArrayList<>();

        int commandNumber = 0;

        //This for loop traverses the arraylist called 'lines' and stores each instance
        //temporarily in the variable 'line'. 
        for (String line: lines) {

            commandNumber++;

            try {
                //Split the line into tokens delimited by ',' and store the tokens in an
                //array of strings.
                if(line.length() != 0)
                {
                    String[] tokens = line.split(DELIMITER);

                    //This function takes the tokens in a singe line, which make up a single 
                    //command, and converts them into a Command object. 
                    Command command = parseCommand(tokens, commandNumber);

                    //adding the command from the current line, and inserts it into the list of 
                    //all commands.
                    commands.add(command);

                }else 
                {
                    //If the current line has length 0, decrement the commandNumber, since 
                    //an empty line does not represent a command.
                    commandNumber--;
                }

            }catch(InvalidCommand e)
            {
                //Adding the string representation of the exception to the list of 
                //exceptions.
                exceptionList.add(e.toString());

            }
        }

        return commands;
    }

    private static Command parseCommand(String[] tokens, int lineNumber) throws InvalidCommand
    {

        try{
            //NB. Any automatic variables declared in the try block, will be deleted
            //in the catch block. That's why we need to include the reurn statement 
            //in the try block, else 'column' and 'row' will become out of scope if 
            //we did the return statement after the cacth block.

            //Function checking the length of the line. 
            //This may throw an InvalidCommand exception. 
            validateLength(tokens, lineNumber); //exception handled. 

            //What these parsing functions do is that they take the part of the string that concerns 
            //them and converts it to the required object. 
            PlayerType player = parsePlayer(tokens[0], lineNumber);
            CommandType commandType = parseCommandType(tokens[1], lineNumber);
            Coordinate source = parseCoord(tokens[2], lineNumber);
            Coordinate destination = parseCoord(tokens[3], lineNumber);

            //Using the newly created objects above, passing them to the constructor of the 
            //command, and returning a newly created command.
            return new Command(player, commandType, source, destination);

        }catch(InvalidCommand e)
        {
            //Rethrowing to parseCommands() method, the one that invoked this method. 
            throw e;
        }
    }

    private static void validateLength(String[] tokens, int lineNumber) throws InvalidCommand {

        //A line should have 4 tokens in total.
        if(tokens.length != 4) 
        {
            //This exception will be eventually caught by the ParseCommands function.
            throw new InvalidCommand(lineNumber, "invalid amount of tokens (" + tokens.length + ")");
        }
    }

    private static PlayerType parsePlayer(String token, int lineNumber) throws InvalidCommand {

        //This removes any leading and trailing whitespaces in the token. 
        String trimmedToken = token.trim();

        PlayerType pt;

        try
        {
            pt = PlayerType.valueOf(trimmedToken);

        }catch(IllegalArgumentException e)
        {
            //Throwing an invalid command exception to parseCommand().
            throw new InvalidCommand(lineNumber, "'" + token + "'" + " is not a valid player");
        }

        //This returns the enum constant of the specified enum type whose name is like the
        //string passed as argument. 
        return pt;
        
    }

    private static CommandType parseCommandType(String token, int lineNumber) throws InvalidCommand {

        String trimmedToken = token.trim();

        CommandType ct;

        try
        {
            ct = CommandType.valueOf(trimmedToken);

        }catch(IllegalArgumentException e)
        {
            //Throwing an invalid command exception to parseCommand().
            throw new InvalidCommand(lineNumber, "'" + token + "'" + " is not a valid command type");
        }

        return ct;
    }

    private static Coordinate parseCoord(String token, int lineNumber) throws InvalidCommand
    {

        String trimmedToken = token.trim();

        if(trimmedToken.length() != 2)
        {
            //This exception will be eventually caught by the ParseCommands() function.
            throw new InvalidCommand(lineNumber, "'" + token + "'" + " is not a valid coordinate");

        }else 
        {
            //This will get the first character in the string which represents the column.
            char columnChar = trimmedToken.charAt(0);

            //converting it to string, and converting it to uppercase just in case it is 
            //in lowercase. 
            String columnString = Character.toString(columnChar);
            columnString = columnString.toUpperCase();

            String rowString = Character.toString(trimmedToken.charAt(1));

            int col;

            try
            {
                //NB. Any automatic variables declared in the try block, will be deleted
                //in the catch block. That's why we need to include the reurn statement 
                //in the try block, else 'column' and 'row' will become out of scope if 
                //we did the return statement after the cacth block.

                if(columnString.equals("A"))
                    col = 0;
                else if(columnString.equals("B"))
                    col = 1;
                else if(columnString.equals("C"))
                    col = 2;
                else if(columnString.equals("D"))
                    col = 3;
                else if(columnString.equals("E"))
                    col = 4;
                else if(columnString.equals("F"))
                    col = 5;
                else if(columnString.equals("G"))
                    col = 6;
                else if(columnString.equals("H"))
                    col = 7;
                else 
                    throw new InvalidCommand(lineNumber, "'" + token + "'" + " has invalid column '" + columnString + "'");

                //reducing one, because our chessboard starts from 0 not 1, and ends at 7 not 8.
                int row = Integer.parseInt(rowString)-1;

                if((row < 0) || (row > 7))
                {
                    throw new InvalidCommand(lineNumber, "'" + token + "'" + " has invalid row '" + rowString + "'");
                }

                //Using the constructor of the Coordinate class to create a new corrdinate object.
                return new Coordinate(row, col);

            }catch(NumberFormatException e)
            {
                //When the string that is supposed to hold the row number, does not 
                //contain a parsable integer.
                throw new InvalidCommand(lineNumber, "'" + token + "'" + " has invalid row '" + rowString + "'");
            }

        }
    }

}