package kasparoop;

import java.io.*;

public class BadMove extends RuntimeException
{
    public String message;

    public BadMove(Coordinate source, Coordinate dest, int indicator)
    {
        //The indicator integer is 1 when the source is not occupied, 2 
        //when the destination is occupied. Else, when the move isn't
        //a valid one.

        //We're adding 1 to the rows of the source and dest, because when reading the 
        //commands from the csv file, we reduced 1 from the rows, since the user 
        //assumes that the row numbers start from 1 and go to 8, while they start
        //from 0 and go to 7. 
        //This amendment here, will reverse the change made when the commands were being 
        //read, and outputs the same coordinate to the user that he entered primarily.
        int sourceRow = source.row + 1;
        int destRow = dest.row + 1;

        if(indicator == 1)
        {
            message = source.getColChar() + sourceRow + " cannot move to " + dest.getColChar() + destRow + " (" + source.getColChar() + sourceRow + " is empty)";

        }else if(indicator == 2)
        {
            message = source.getColChar() + sourceRow + " cannot move to " + dest.getColChar() + destRow + " (" + dest.getColChar() + destRow + " is occupied)";

        }else
        {
            message = source.getColChar() + sourceRow + " cannot move to " + dest.getColChar() + destRow;
        }
    }
}