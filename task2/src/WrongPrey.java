package kasparoop;

import java.io.*;

public class WrongPrey extends RuntimeException
{
    public String message;

    public WrongPrey(Coordinate coord)
    {
        message = "The ChessPiece at " + coord.toString() + " is not the one intended to be killed";
    }
}