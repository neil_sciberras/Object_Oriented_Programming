//An abstract class that every other chess piece will extend. 

package kasparoop;

import java.lang.Character;

abstract class ChessPiece
{
    //protected means that subclasses will have access
    protected int id;
    public Coordinate location;
    public PlayerType owner;
    protected boolean alive;

    //this field will be set in the inherited class, since it is different for every type of 
    //chesspiece. But once it is set, its value cannot be changed since its final.
    public final char symbol;

    public ChessPiece(char symbol, int id, Coordinate location, PlayerType owner)
    {
        //The black player will ave his piece in ippercase, and the white player 
        //will have his pieces in lowercase.
        if(owner == PlayerType.W)
            symbol = Character.toLowerCase(symbol);

        this.symbol = symbol;
        this.id = id;
        this.location = location;
        this.owner = owner;
        alive = true;
    }

    public void kill()
    {
        alive = false;
    }

    public boolean outOfBounds(Coordinate coord)
    {
        int row = coord.row;
        int col = coord.col;

        if(((row >= 0) && (row <= 7)) && ((col >= 0) && (col <= 7)))
            return false;
        else
            return true;
    }

    //This applies to all chess pieces, execept to the Pawn, which needs to override this 
    //method with another implementation.
    public boolean canKill(ChessBoard chessB, Coordinate attackingCoord)
    {
        int row = attackingCoord.row;
        int col = attackingCoord.col;

        //requirement 6 of assignemnt spec list.
        //If the killer can move to that position
        if(validMove(chessB, attackingCoord))
        {
            //requirement 5 of assignment spec list 
            //"An EAT command requires an opponent'schess piece at the destination coordinate."

            //and, if the box being attacked is taken by some piece (not free)
            if(chessB.check(row, col))
            {
                //and, if the killer and the victim have different owners
                if(chessB.boxOwner(row, col) != this.owner)
                {
                    return true;
                }
            }
        }

        //if this part is reached, then the previous conditions haven't 
        //been met, so we return false; 
        return false;
    }

    //This will be used by those chesspieces that need to check if there is 
    //any chesspiece in the way, in the diagonal. 
    //It checks the amount of diagonal steps specified in the argument.
    //It returns true if there's nothing in the way, and false otherwise.
    //Access modifier Protected, since this will only be used by the 
    //subclasses and not from outer classes.
    protected boolean checkDiagonalPath(ChessBoard board, Coordinate destination, int steps)
    {
        int sourceRow = location.row;
        int sourceCol = location.col;
        int destRow = destination.row;
        int destCol = destination.col;

        boolean northE = false;
        boolean northW = false;
        boolean southE = false;
        boolean southW = false;

        if((sourceRow < destRow))
        {
            //SOUTH

            if(sourceCol < destCol)
                southE = true;
            else
                southW = true;

        }else //sourcerow is further down than the destcol, hence north
        {
            //NORTH
            if(sourceCol < destCol)
                northE = true;
            else
                northW = true;
        }

        Box currentBox;

        //until 'i' is smaller than steps, so ta avoid checking the last step.
        //The last step would include checking the destination itself. But any checks 
        //needed on the destination would have been made previously in the 
        //chessboard class methods kill() and move().
        //If we check the destination itself, when this method is used to check for a 
        //valid kill, then the destination would obviously be taken, because else
        //there would be no one to kill.
        for(int i = 1; i < steps; i++)
        {
            //getting the next box in the diagonal way according to the switched on flag

            if(southE)
            {
                currentBox = board.boxAt(sourceRow+i, sourceCol+i);

            }else if(southW)
            {
                currentBox = board.boxAt(sourceRow+i, sourceCol-i);

            }else if(northE)
            {
                currentBox = board.boxAt(sourceRow-i, sourceCol+i);

            }else //if(northW)
            {
                currentBox = board.boxAt(sourceRow-i, sourceCol-i);
            }

            if(currentBox.isTaken())
            {
                System.out.println("here");
                return false;
            }
        }

        //If this line has been reached, then there is no pieces in the way, 
        //hence return true.
        return true;
    }

    protected boolean checkHoriVertPath(ChessBoard board, Coordinate destination)
    {
        int sourceRow = location.row;
        int sourceCol = location.col;
        int destRow = destination.row;
        int destCol = destination.col;

        if(sourceRow == destRow) //horizontal
        {
            int steps = Math.abs(destCol-sourceCol);
            return checkHorizontalPath(board, destination, steps);

        }else //since in the method that invoked this method, it has already been 
        //ensured that EITHER the rows or columns are equal, then if its not the 
        //rows, its obviously the columns. When columns are the same, means that the 
        //path is vertical.
        {
            int steps = Math.abs(destRow-sourceRow);
            return checkVerticalPath(board, destination, steps);
        } 
    }

    private boolean checkVerticalPath(ChessBoard board, Coordinate destination, int steps)
    {
        int sourceRow = location.row;
        int sourceCol = location.col;
        int destRow = destination.row;
        int destCol = destination.col;

        boolean down = false;
        boolean up = false;

        //deciding which type of vertical it is, either upwards or downwards.
        if(sourceRow < destRow)
            down = true;
        else
            up = true;

        Box currentBox;
            
        for(int i = 1; i < steps; i++)
        {
            if(down)
            {
                //gets the next box in the vertical way from the source to the destination
                currentBox = board.boxAt(sourceRow+i, sourceCol);
            }else //if up is true
                currentBox = board.boxAt(sourceRow-i, sourceCol);

            if(currentBox.isTaken())
                return false;
        }

        //If this line has been reached, then there is no pieces in the way, 
        //hence return true.
        return true;

    }

    private boolean checkHorizontalPath(ChessBoard board, Coordinate destination, int steps)
    {
        int sourceRow = location.row;
        int sourceCol = location.col;
        int destRow = destination.row;
        int destCol = destination.col;

        boolean left = false;
        boolean right = false;

        //deciding which type of horizontal it is, either left or right.
        if(sourceCol < destCol)
            right = true;
        else
            left = true;

        Box currentBox;
            
        for(int i = 1; i < steps; i++)
        {
            if(right)
            {
                //gets the next box in the horizontal way from the source to the destination
                currentBox = board.boxAt(sourceRow, sourceCol+i);
            }else //if left is true
                currentBox = board.boxAt(sourceRow, sourceCol-i);

            if(currentBox.isTaken())
                return false;
        }

        //If this line has been reached, then there is no pieces in the way, 
        //hence return true.
        return true;

    }


    abstract boolean validMove(ChessBoard board, Coordinate destination);





    //The following methods are put here instead of in their respective subclasses, to be 
    //able to reuse the code in the queen class. 
    //Since the queen's valid move is just an 'OR' combination of the bishop's and the 
    //rook's 'valid moves'.
    //They're protected, since they're only going to be called from the subclasses
    //of this class. 
    protected boolean bishopValidMove(ChessBoard board, Coordinate destination)
    {
        if(!outOfBounds(destination))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int destRow = destination.row;
            int destCol = destination.col;

            //These two integers hold the differnece between the column number and 
            //row number of the source and destination.
            int colDifference = Math.abs(destCol-sourceCol);
            int rowDifference = Math.abs(destRow-sourceRow);

            //The row difference and the column difference should be equal, so that 
            //the bishop is allwoed to move only in diagonal movement.
            //But if either one of the differences is zero, then it means it is not diaginal.

            if(colDifference == 0)
                return false;
            else if(colDifference == rowDifference)
                return checkDiagonalPath(board, destination, colDifference);
            else
                return false; 

        }else
            return false; 
    }

    protected boolean rookValidMove(ChessBoard board, Coordinate destination)
    {
        if(!outOfBounds(destination))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int destRow = destination.row;
            int destCol = destination.col;

            //these two methods ensure that the destination has the same column or has the
            //same row.
            if(checkCol(sourceCol, destCol) || checkRow(sourceRow, destRow))
            {
                //if BOTH the rows and the columns are the same for both the source and the 
                //destination, then they describe the same box. This will lead to errors, 
                //so we rule it out here. 
                if(!((sourceRow == destRow) && (sourceCol == destCol)))
                    return checkHoriVertPath(board, destination);
                else
                    return false; 
            }
            else
                return false; 

            
        }else
            return false;
    }

    protected boolean checkCol(int sourceCol, int destCol)
    {
        //If the destination is on the same col
        if((destCol == sourceCol))
        {
            return true;

        }else
            return false; 
    }

    protected boolean checkRow(int sourceRow, int destRow)
    {
        //If the destination is on the same row
        if((destRow == sourceRow))
        {
            return true;

        }else
            return false; 
    }

}