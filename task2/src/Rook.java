package kasparoop;

public class Rook extends ChessPiece
{
    public Rook(int id, Coordinate location, PlayerType owner)
    {
        super('R', id, location, owner); //invoking the constructor of ChessPiece
    }

    public boolean validMove(ChessBoard board, Coordinate destination)
    {
        return rookValidMove(board, destination);
    }
}