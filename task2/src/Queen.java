package kasparoop;

public class Queen extends ChessPiece
{
    public Queen(int id, Coordinate location, PlayerType owner)
    {
        super('Q', id, location, owner); //invoking the constructor of ChessPiece
    }

    boolean validMove(ChessBoard board, Coordinate destination)
    {
        //Since the queen can move horizontally, vertically, and in diagonals too, 
        //we can 'OR' the booleans returned by the 'validMove' of the Rook and the 
        //Bishop.
        
        boolean horiVert = rookValidMove(board, destination);
        boolean diagonals = bishopValidMove(board, destination);

        return (horiVert || diagonals);
    }
}