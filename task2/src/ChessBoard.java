package kasparoop;

public class ChessBoard
{
    private Box board[][] = new Box[8][8];
    public int pawnUsage, bishopUsage, knightUsage, rookUsage, queenUsage, kingUsage;

    //Constructor 
    public ChessBoard()
    {
        pawnUsage = 0;
        bishopUsage = 0;
        knightUsage = 0;
        rookUsage = 0;
        queenUsage = 0;
        kingUsage = 0;

        int counter = 0;

        for(int row = 0; row < 8; row++)
        {
            for(int col = 0; col < 8; col++)
            {
                board[row][col] = new Box(row, col);

                PlayerType owner;

                if((row == 1) || (row == 6))
                {
                    //Initialising the Pawns and assigning them to the right positions 

                    owner = designatedOwner(row);

                    Coordinate coord = new Coordinate(row, col);                    

                    board[row][col].assign(new Pawn(counter, coord, owner));

                }else if((row == 0) || (row == 7))
                {
                    //Initialising the Rooks and assigning them to the right positions 
                    if((col == 0) || (col == 7))
                    {
                        owner = designatedOwner(row);

                        Coordinate coord = new Coordinate(row, col);                    

                        board[row][col].assign(new Rook(counter, coord, owner));
                    }else if((col == 1) || (col == 6))
                    {
                        //Initialising the Knights and assigning them to the right positions 
                        owner = designatedOwner(row);

                        Coordinate coord = new Coordinate(row, col);                    

                        board[row][col].assign(new Knight(counter, coord, owner));
                    }else if((col == 2) || (col == 5))
                    {
                        //Initialising the Knights and assigning them to the right positions 
                        owner = designatedOwner(row);

                        Coordinate coord = new Coordinate(row, col);                    

                        board[row][col].assign(new Bishop(counter, coord, owner));
                    }else if(col == 3)
                    {
                        //Initialising the Knights and assigning them to the right positions 
                        owner = designatedOwner(row);

                        Coordinate coord = new Coordinate(row, col);                    

                        board[row][col].assign(new Queen(counter, coord, owner));
                    }else if(col == 4)
                    {
                        //Initialising the Knights and assigning them to the right positions 
                        owner = designatedOwner(row);

                        Coordinate coord = new Coordinate(row, col);                    

                        board[row][col].assign(new King(counter, coord, owner));
                    }
                }

                counter++;
            }
        }
    }

    //returns the owner of a PlayerType object, according to the row
    private PlayerType designatedOwner(int row)
    {
        if((row == 1) || (row == 0))
            return PlayerType.valueOf("B");
        else //if((row == 7) || (row == 6))
            return PlayerType.valueOf("W");
    }

    public boolean check(int row, int col)
    {
        return board[row][col].isTaken();
    }

    public PlayerType boxOwner(int row, int col)
    {
        return board[row][col].boxOwner();
    }

    public Box boxAt(int row, int col)
    {
        return board[row][col];
    }


    public void printBoard()
    {
        System.out.println("                  A   B   C   D   E   F   G   H");

        for(int row = 0; row < 8; row++)
        {
            System.out.println("                ---------------------------------");

            int dispRow = row+1;

            if(row == 0)
                System.out.print("Black Side   " + dispRow);
            else if(row == 7)
                System.out.print("White Side   " + dispRow);
            else
                System.out.print("             " + dispRow);

            for(int col = 0; col < 8; col++)
            {
                char symbol = board[row][col].symbol;

                if(col == 7)
                {
                    System.out.print(" " + symbol + " |");
                    System.out.println();

                }else if(col == 0)
                {
                    System.out.print("  | " + symbol + " |");
                }
                else
                {
                    System.out.print(" " + symbol + " |");
                }
            }

            if(row == 7)
                System.out.println("                ---------------------------------");
        }
    }

    public boolean move(Coordinate source, Coordinate destination) throws BadMove
    {
        Box sourceBox = board[source.row][source.col];
        Box destBox = board[destination.row][destination.col];

        //requirement 2 of assignemnt spec list 
        //"A chesspiece must exist at the source coordinate."
        if((sourceBox.isTaken()) && !(destBox.isTaken()))
        {
            //'this' is refering to the chessboard we're in right now
            if(sourceBox.piece.validMove(this, destination))
            {
                ChessPiece piece = sourceBox.piece;

                destBox.assign(piece);
                sourceBox.free();

                //System.out.println(source + " -> " + destination);

                //incrmenting the usage just for statistics
                incrementUsage(piece);

                //this shows that the command has been carried out successfully
                return true;
            }else
            {
                int sourceRow = source.row + 1;
                int destRow = destination.row + 1;

                System.out.println(source.getColChar() + sourceRow + " cannot move to " + destination.getColChar() + destRow);

                //command not carried out successfully
                return false;
            }
        }else
        {

            if(!sourceBox.isTaken())
                throw new BadMove(source, destination, 1);
            else
                throw new BadMove(source, destination, 2);
        } 
    }

    public boolean kill(Coordinate source, Coordinate destination) throws BadKill
    {
        Box sourceBox = board[source.row][source.col];
        Box destBox = board[destination.row][destination.col];

        if(sourceBox.piece.canKill(this, destination))
        {
            ChessPiece killer = sourceBox.piece;
            ChessPiece victim = destBox.piece;

            //incrementing the according counter, whether the pieces are 
            //pawn, bishop, etc.
            incrementUsage(killer);
            incrementUsage(victim);
            
            //This sets the 'alive' flag to false, so it is declared as killed.
            killer.kill();

            //removing the piece at the destination
            destBox.free();

            //assigning the piece that was at the source to the destination box
            destBox.assign(killer);

            //removing the piece from the source
            sourceBox.free();

            //System.out.println(source + " X " + destination);

            //command successfully carried out
            return true;
            
        }else
        {
            throw new BadKill(source, destination);
        } 
    }

    public int score(PlayerType player)
    {
        int total = 0;

        Box box;
        ChessPiece boxPiece;

        for(int row = 0; row < 8; row++)
        {
            for(int col = 0; col < 8; col++)
            {
                box = board[row][col];

                //if the box is occupied
                if(box.isTaken())
                {
                    boxPiece = box.piece;

                    //if the box is owned by the player passed as argument
                    if(box.boxOwner() == player)
                    {
                        if(boxPiece instanceof Pawn)
                            total += 1;
                        else if(boxPiece instanceof Bishop)
                            total += 3;
                        else if(boxPiece instanceof Knight)
                            total += 3;
                        else if(boxPiece instanceof Rook)
                            total += 5;
                        else if(boxPiece instanceof Queen)
                            total += 9;
                        else if(boxPiece instanceof King)
                            total += 20;
                    }
                }

            }
        }

        return total;
    }

    private void incrementUsage(ChessPiece piece)
    {
        if(piece instanceof Pawn)
            pawnUsage++;
        else if(piece instanceof Bishop)
            bishopUsage++;
        else if(piece instanceof Knight)
            knightUsage++;
        else if(piece instanceof Rook)
            rookUsage++;
        else if(piece instanceof Queen)
            queenUsage++;
        else if(piece instanceof King)
            kingUsage++;
    }
}