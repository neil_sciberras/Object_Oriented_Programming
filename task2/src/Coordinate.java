package kasparoop;

import kasparoop.Column;

public class Coordinate {

    public int row;
    public int col;

    public Coordinate(int row, int col) {
        this.col = col;
        this.row = row;
    }

    public String getColChar()
    {
        switch(col) 
        {
            //No breaks needed, because the return will surely exit the method
            case 0: return "A" ;
            case 1: return "B" ;
            case 2: return "C" ;
            case 3: return "D" ;
            case 4: return "E" ;
            case 5: return "F" ;
            case 6: return "G" ;
            case 7: return "H" ;
            default: return "" + col + row;
        }
    }

    @Override
    public String toString() 
    {
        return getColChar() + row;
    }
}