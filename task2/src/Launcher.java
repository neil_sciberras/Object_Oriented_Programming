package kasparoop;

import java.util.List;
import java.util.ArrayList;
import java.io.IOException;

public class Launcher {

    public static void main(final String[] args) {

        //if no command line arguments are passed
        if(args.length == 0) 
        {
            System.out.println("No arguments have been passed.");
            System.out.println("Proper usage: java programName arguments");
        }else 
        {
            //Printing the chess boar with its initial set up.
            System.out.println();
            System.out.println("Initial set up of the chess board:");
            System.out.println();
            System.out.println();
            ChessBoard board = new ChessBoard();
            board.printBoard();


            int appliedCommands = 0;
            int invalidSkippedCommands = 0;

            //List of strings to hold the caught exceptions, and then output them all at once. 
            List<String> InvalidCommands = new ArrayList<String>();

            try{
            
                final List<Command> commands = CommandReader.readCommands(args[0], InvalidCommands);

                System.out.println();
                System.out.println();
                System.out.println("-------------------------------------------------");
                System.out.println("Printing all syntactically valid Commands:");
                System.out.println("-------------------------------------------------");

                for(final Command command: commands) {
                    //Printing each command. 'System.out.println(command)' calls the object's 
                    //toString() fucntion to convert the object to a string representation. 
                    System.out.println(command);
                }
                System.out.println();


                //this has to be inside the try catch, because else the commands list would 
                //go out of scope.
                for(final Command command: commands) 
                {
                    int sourceRow = command.getSource().row;
                    int sourceCol = command.getSource().col;
                    int destRow = command.getDestination().row;
                    int destCol = command.getDestination().col;

                    PlayerType sourceOwner = board.boxOwner(sourceRow, sourceCol);

                    //if the source is not empty
                    if(sourceOwner != null)
                    {
                        //requirement 3 of assignment spec list
                        if(command.getPlayer() == sourceOwner)
                        {
                            //Calling the right method inside chessboard class, according
                            //to the commad type.

                            if(command.getCommandType() == CommandType.MOVE)
                            {
                                if(board.move(command.getSource(), command.getDestination()))
                                    appliedCommands++;
                                else
                                    invalidSkippedCommands++;
                            
                            }else if(command.getCommandType() == CommandType.EAT)
                            {
                                if(board.kill(command.getSource(), command.getDestination()))
                                    appliedCommands++;
                                else
                                    invalidSkippedCommands++; 
                            }

                        }else
                            System.out.println("Owner of command '" + command + "' is not the same as the source's owner");
                    }else 
                        System.out.println("In command  '" + command + "', source is empty");
                }

            }catch(IOException e) 
            {
                //This is handling the exception thrown by the readAllLines method invoked 
                //inside the readCommands method.

                System.out.println("Error reading file: " + args[0]);
            }catch(BadKill e)
            {
                System.out.println(e.message);
            }catch(BadMove e)
            {
                System.out.println(e.message);
            }

            //If the number of exceptions caught is not zero
            if(InvalidCommands.size() != 0)
            {
                System.out.println();
                System.out.println("-------------------------------------------------");
                System.out.println("Printing all invalid commands:");
                System.out.println("-------------------------------------------------");

                for(String InvalidCommand : InvalidCommands)
                {
                    System.out.println(InvalidCommand);
                }
            }

            //adding the ill-formed (invalid commands) tot eh counter of skipped commands
            invalidSkippedCommands += InvalidCommands.size();

            System.out.println();
            System.out.println("Number of applied commands: " + appliedCommands);
            System.out.println("Number of invalid/skipped commands: " + invalidSkippedCommands);
            
            if(board.score(PlayerType.B) > board.score(PlayerType.W))
            {
                System.out.println("Winning player: BLACK");

            }else if(board.score(PlayerType.B) < board.score(PlayerType.W))
            {
                System.out.println("Winning player: WHITE");

            }else //when equal scores
            {
                System.out.println("Winning player: TIE");
            }


            //deciding which is the largest usageCounter.
            String pieceType = "PAWN";
            int max = board.pawnUsage;

            if(board.bishopUsage > max)
            {
                max = board.bishopUsage;
                pieceType = "BISHOP";
            }
            
            if(board.knightUsage > max)
            {
                max = board.knightUsage;
                pieceType = "KNIGHT";
            } 

            if(board.rookUsage > max)
            {
                max = board.rookUsage;
                pieceType = "ROOK";
            }
            
            if(board.queenUsage > max)
            {
                max = board.queenUsage;
                pieceType = "QUEEN";
            }

            if(board.kingUsage > max)
            {
                max = board.kingUsage;
                pieceType = "KING";
            }

            System.out.println("Most used chess piece: " + pieceType + ", " + max);

            System.out.println();
            System.out.println("Last chess board state:");
            System.out.println();
            board.printBoard();

        }
    }
}
