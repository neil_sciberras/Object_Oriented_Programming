package kasparoop;

import java.io.*;

public class InvalidCommand extends RuntimeException
{
    private int lineNumber;
    private String reason;

    public InvalidCommand(int lineNumber, String reason)
    {
        this.lineNumber = lineNumber;
        this.reason = reason;
    }

    public int getLineNumber()
    {
        return lineNumber;
    }

    @Override
    public String toString() {
        return "Command number " + lineNumber + ": " + reason;
    }
}