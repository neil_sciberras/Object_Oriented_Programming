//This represents one box in a chess board.

package kasparoop;

public class Box
{
    final Coordinate coord;
    private boolean taken;
    public ChessPiece piece;
    char symbol;

    public Box(int row, int col)
    {
        //When initiated, the box is still free and has no chesspiece on it.
        taken = false;
        piece = null;
        symbol = '-';
        coord = new Coordinate(row, col);
    }

    public boolean isTaken()
    {
        return taken;
    }

    public PlayerType boxOwner()
    {
        if(piece != null)
            return piece.owner;
        else
            return null; 
    }

    public void assign(ChessPiece piece) throws BoxTaken
    {
        //If the piece being assigned to this box is a pawn
        if(piece instanceof Pawn)
        {
            //If the pawn is being assigned to any other box rather than its initial
            //position, hence being moved, then the 'moved' flag has to be set to true, 
            //because after the first movement of a pawn, its movement will be different. 
            if((coord.row != 1) && (coord.row != 6))
            {
                //Downcasting the piece to a pawn object, so that its 'moved' boolean 
                //field can be accessed.
                Pawn pawnPiece = (Pawn) piece;

                pawnPiece.moved = true;
            }
        }

        //It can be assigned to a chesspiece only if it is free.
        if(!taken)
        {
            taken = true;

            //this box's piece will be equal to the piece that was assigned to it.
            this.piece = piece;
            symbol = piece.symbol;

            //Setting the location of the piece assigned to this box, equal 
            //to the coordinates of this box.
            piece.location = coord;
        }else
        {
            throw new BoxTaken(coord);            
        }
    }

    public void free()
    {
        taken = false;
        piece = null;
        symbol = '-';
    }

    public void kill(ChessPiece killer, ChessPiece prey) throws WrongPrey
    {
        if(taken)
        {
            //If the intended prey is actually the piece present at this box, then go on
            //with the killing, else throw exception. 
            if(prey.id == piece.id)
            {
                //Replacing the prey by the killer.
                piece = killer;
                symbol = killer.symbol;
                prey.alive = false;

            }else
            {
                throw new WrongPrey(coord);
            }
        }
    }
}