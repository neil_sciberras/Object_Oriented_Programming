package kasparoop;

public class Bishop extends ChessPiece 
{
    public Bishop(int id, Coordinate location, PlayerType owner)
    {
        super('B', id, location, owner); //invoking the constructor of ChessPiece
    }

    public boolean validMove(ChessBoard board, Coordinate destination)
    {
        return bishopValidMove(board, destination);
    }
}