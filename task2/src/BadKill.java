package kasparoop;

import java.io.*;

public class BadKill extends RuntimeException
{
    public String message;


    public BadKill(Coordinate source, Coordinate dest)
    {
        //We're adding 1 to the rows of the source and dest, because when reading the 
        //commands from the csv file, we reduced 1 from the rows, since the user 
        //assumes that the row numbers start from 1 and go to 8, while they start
        //from 0 and go to 7. 
        //This amendment here, will reverse the change made when the commands were being 
        //read, and outputs the same coordinate to the user that he entered primarily.
        source.row += 1;
        dest.row += 1;

        message = source + " cannot kill " + dest;
    }
}