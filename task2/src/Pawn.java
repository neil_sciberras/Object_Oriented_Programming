package kasparoop;

public class Pawn extends ChessPiece
{
    //When the pawn is moved once, this is set to true. 
    //This is useful since the pawn has a different first move from the rest of the moves.
    public boolean moved;

    public Pawn(int id, Coordinate location, PlayerType owner)
    {
        super('P', id, location, owner); //invoking the constructor of ChessPiece
        moved = false;
    }

    boolean validMove(ChessBoard board, Coordinate destination)
    {
        
        if(!outOfBounds(destination))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int destRow = destination.row;
            int destCol = destination.col;

            if(checkMoveCol(sourceCol, destCol))
            {
                //When the pawn has already moved, then a valid move would be one 
                //box away only. The move() in the ChessBoard class takes care 
                //of checking whether the destination box is free or not.
                if(moved)
                {
                    //The black start from the top end of the board, and move downwards.
                    if(owner == PlayerType.B)
                    {
                        //if the destination is one box downwards, and the destination box is not 
                        //taken by anyone
                        if(destRow == sourceRow+1)
                        {
                            return true;
                        }else
                        {
                            return false;
                        }
                    }else
                    {
                        //if the destination is one box upwards, and the destination box is not 
                        //taken by anyone
                        if(destRow == sourceRow-1)
                        {
                            return true;
                        }else
                        {
                            return false;
                        }
                    }

                }else
                {
                    //When the pawn has not yet moved, then its first move would be of two 
                    //boxes not one. 
                    //The black start from the top end of the board, and move downwards.
                    if(owner == PlayerType.B)
                    {
                        //if the destination is two boxes downwards, and the destination box is not 
                        //taken by anyone
                        if(destRow == sourceRow+2)
                        {
                            //checking whether the box in between the source and destination
                            //is free, hence the path is free.
                            if(!board.check(sourceRow+1, destCol))
                                return true;
                            else
                                return false; 
                        }else
                        {
                            return false;
                        }
                    }else
                    {
                        //if the destination is two boxes upwards, and the destination box is not 
                        //taken by anyone
                        if(destRow == sourceRow-2)
                        {
                            if(!board.check(sourceRow-1, destCol))
                                return true;
                            else
                                return false;
                        }else
                        {
                            return false;
                        }
                    }

                } 
                
            }else
                return false; 
        }else
            return false;
    }

    private boolean checkMoveCol(int sourceCol, int destCol)
    {
        //If the destination is on the same column
        if((destCol == sourceCol))
        {
            return true;

        }else
            return false; 
    }

    //this method is only used from inside this class, hence private.
    private boolean checkAttackCol(int sourceCol, int attackCol)
    {
        //If the destination is 1 column to the right 
        if((attackCol == sourceCol+1))
        {
            return true;

        }else if(attackCol == sourceCol-1)
        {
            //If the destination is 1 column to the left 

            return true;

        }else
            return false; 
    }

    private boolean takenByEnemy(ChessBoard chessB, int row, int col)
    {
        //If the destination to be attacked is taken
        if(chessB.check(row, col))
        {
            if(chessB.boxOwner(row, col) != this.owner)
            {
                return true;
            }else
                return false;
        }else
            return false;
    }

    @Override
    public boolean canKill(ChessBoard chessB, Coordinate attackingCoord)
    {
        if(!outOfBounds(attackingCoord))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int attackRow = attackingCoord.row;
            int attackCol = attackingCoord.col;

            //This returns true if the column of the attacking destination is one column 
            //away only from the source (right or left).
            if(checkAttackCol(sourceCol, attackCol))
            {
                //The black start from the top end of the board, and move downwards.
                if(owner == PlayerType.B)
                {
                    //If the destination is 1 row downwards
                    if((attackRow == sourceRow+1))
                    {
                        if(takenByEnemy(chessB, attackRow, attackCol))
                            return true;
                        else
                            return false; 

                    }else 
                        return false;

                }else
                {
                    //White

                    //If the destination is 1 row upwards
                    if((attackRow == sourceRow-1))
                    {
                        if(takenByEnemy(chessB, attackRow, attackCol))
                            return true;
                        else
                            return false;

                    }else 
                        return false;
                } 
                
            }else
                return false; 

            

        }else
            return false; 
    }

}