package kasparoop;

import java.lang.Math;

public class King extends ChessPiece
{
    public King(int id, Coordinate location, PlayerType owner)
    {
        super('K', id, location, owner); //invoking the constructor of ChessPiece
    }

    boolean validMove(ChessBoard board, Coordinate destination)
    {
        if(!outOfBounds(destination))
        {
            int sourceRow = location.row;
            int sourceCol = location.col;
            int destRow = destination.row;
            int destCol = destination.col;

            //These two integers hold the differnece between the column number and 
            //row number of the source and destination.
            int colDifference = Math.abs(destCol-sourceCol);
            int rowDifference = Math.abs(destRow-sourceRow);

            //Note that the checking of whether the destination is occupied or not, 
            //is being made before this method is invoked ( in the move() function 
            //in the ChessBoard class).

            if(colDifference == 1)
            {
                //When the column is one to the right or to the left, then the row can be
                //either the same as the source, or with a difference of 1.

                if((rowDifference == 0) || (rowDifference == 1))
                    return true;
                else
                    return false; 

            }else if(colDifference == 0)
            {
                //When the destination is on the same column as the source, then the 
                //riw difference can only be of 1.

                if(rowDifference == 1)
                    return true;
                else
                    return false; 
            }else //the column difference can only be of 1 or 0.
                return false; 

        }else
            return false; 
    }
}