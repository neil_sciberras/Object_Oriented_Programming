#include "InvalidPiece.h"

InvalidPiece::InvalidPiece(string type)
{
    stringstream stream;
    stream << "'" << type << "' is an invalid lego piece!";
    
    message = stream.str();
}