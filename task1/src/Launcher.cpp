//Reference used to understand better heap and stack:
//  http://www.learncpp.com/cpp-tutorial/79-the-stack-and-the-heap/

//Memory leaks: https://stackoverflow.com/questions/6261201/how-to-find-memory-leak-in-a-c-code-project

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "csv_reader.h"
#include "LL.h"
#include "LegoPiece.h"
#include "Brick.h"
#include "Door.h"
#include "Window.h"
#include "Quarry.h"
#include "City.h"
#include "OneSH.h"
#include "TwoSH.h"
#include "Church.h"
#include "Hospital.h"
#include "University.h"
#include "Bob.h"
#include "Peter.h"
#include "Jane.h"

using namespace std;

int main(int argc, char ** argv)
{
    cout << endl;

    string filePath;

    //quarryStore is a pointer that points to an object on the heap. It is important that we delete it
    //when we are ready form its use. 
    Quarry *quarryStore = new Quarry;
    //cityStore needs to be deleted as well. 
    City *cityStore = new City;

    //these will hold the amount of buildings requested and the amount actually 
    //built, so then we can output the summary in the final stage.
    int requestedNumber = 0;
    int builtNumber = 0;
    float percentage;

    int specNum = 0;

    //when no input file is passed as command line argument
    if(argc < 2)
    {
        cout << "Please enter 'Specification File' path:" << endl;
        cin >> filePath;
    }else 
    {
        filePath = argv[1];
    }

    try
    {
        //Note that the second and third parameters of the constructor ofCsvReader have default values. 
        CsvReader reader(filePath);
        //This sets the 'inputStream' to filePath
        //          the 'delimeter' to ","
        //          the 'ignoreComments' to false, so the comments are absorbed and not 
        //          thrown away.
        
        vector<string> vec;

        //these flags will indicate in which part of the specification file we are. 
        //Example if we are in the first part, the quarry part, then the quarryFlag will
        //be true, and the buildingsFlag will be false. 
        bool quarryFlag = false;
        bool buildingsFlag = false;

        //these variables will hold the amount of pieces and amount of buildings in every 
        //line of the specification file. 
        int pieceQuantity = 0;
        int oneSHQuant, twoSHQuant, churchQuant, hospitalQuant, universityQuant = 0;
        int buildQuantity = 0;

        int integerToken;
        bool validQuantity = false;
        bool validStringToken = false;

        Bob * contractorBob = new Bob;
        Peter * contractorPeter = new Peter;
        Jane * contractorJane = new Jane;

        //readLine will return false at the EOF
        while (reader.readLine(vec))
        {
            //As soon as this while loop is entered, the vector would have been populated. 

            specNum++;

            //With every line, this is set back to false;
            validStringToken = false;

            //This is done for every line
            int numTokens = vec.size();

            if(numTokens == 0)
            {
                //when the line is empty, there is no specification, so we do not count it 
                //as a spec.
                specNum--;

                //When no tokens are found in the line we're at, we can skip to the next line.
                continue;

            }else if(numTokens == 1)
            {
                //When the number of tokens is 1, it is either the "#quarry" or "#buildings" 
                //header thingy. We check which one it is, and set the flags accordingly, so 
                //as to be able to input the tokens that follow in the right list (or whatever 
                //structure is being used).

                //if the token is equal to "#quarry"
                if(vec[0].compare("#quarry") == 0)
                {
                    quarryFlag = true;
                    buildingsFlag = false;

                }else if(vec[0].compare("#buildings") == 0)
                {
                    quarryFlag = false;
                    buildingsFlag = true;
                }else
                {
                    cout << "   Specification number: " << specNum << " - invalid" << endl << endl;
                }
            }else if(numTokens == 2)
            {
                //This bool is always set to false, so we can detect any invalid tokens. 
                //Because if it was left true from the previous iteration of the loop, then
                //if the token of this iteration is invalid we would not notice.
                validQuantity = false;

                try
                {
                    //Attempting to convert the 2nd token from a string to an integer
                    //If it raises an exception, then the catch will handle it.
                    integerToken = stoi(vec[1]);

                    //If it was successfully converted into an integer, then check its
                    //range, that it's actually between 1 and 100.
                    if((integerToken >= 1) && (integerToken <= 100))
                        validQuantity = true;

                }catch(invalid_argument)
                {
                    //When its invalid, we just jump to the next iteration of the loop, 
                    //the next line of the spec file.
                    cout << "   Specification number: " << specNum << " - invalid" << endl << endl;
                    continue;
                }

                //If the token is infact an integer, but not within the range, then we
                //jump to the next line. 
                if(!validQuantity)
                {
                    cout << "   Specification number: " << specNum << " - invalid (" << integerToken << " is not in the range 1-100)" << endl << endl;
                    continue;
                }

                //When the number of tokens is not 1, then we check where we are, 
                //either in the quarry or the buildings part, and act accordingly. 

                if(quarryFlag)
                {
                    //The second token is always the quantity value, so we just convert it
                    //to an integer. 
                    pieceQuantity = stoi(vec[1]);

                    //We create a pointer to a LegoPiece object, and then we change what 
                    //the pointer points to accoridngly. 
                    //Also, this pointer will take on several forms (it is a polyphormic 
                    //object), namely Door, Window and Brick. 
                    //This pointer will then be passed to the append() function.
                    LegoPiece *tempLegoPiece;

                    /*THIS DYNAMIC CAST WAS SUCCESSFUL

                    //TESTING THE DYNAMIC CAST LOGIC TO GET USED TO IT
                    LegoPiece *temp1 = new Window;

                    if(dynamic_cast<Window*>(temp1) != NULL)
                        cout << "Dynamic cast successful!!!!" << endl;
                    else
                        cout << "Dynamic cast unsuccessful!!!" << endl;
                    
                    */

                    //This FOR loop traverses the vector which contains the tokens, and 
                    //for every token, check what type of legoPiece it is, creates the 
                    //appropriate LegoPiece, and sets the tempLegoPiece pointer to point to it. 
                    for(int i = 0; i < pieceQuantity; i++)
                    {
                        //Whether the specification file contains "brick" or "bricks", 
                        //'find' will look for "brick", which is present in both cases. 
                        if(vec[0].find("brick") != string::npos)
                        {
                            //creating the new object and set the previously created 
                            //pointer to point to it.
                            tempLegoPiece = new Brick;
                            validStringToken = true;
                        }
                        else if(vec[0].find("door") != string::npos)
                        {
                            tempLegoPiece = new Door;
                            validStringToken = true;
                        }
                        else if(vec[0].find("window") != string::npos)
                        {
                            tempLegoPiece = new Window;
                            validStringToken = true;
                        }
                        else
                        {
                            cout << "   Specification number: " << specNum << " - invalid" << endl << endl;
                        }
                            

                        //Adding the newly created LegoPiece object pointed to by 
                        //tempLegoPiece to the Quarry linked list. 
                        //Passing the pointer pointing to the object, to the append function. 
                        //This pointer is the pointer that points to the original and 
                        //only item, so its important not to delete it. 
                        //This means that the pointer that will be present in the linked 
                        //list, which points to the actual data, is really and truly 
                        //pointing to this object cretaed just now. 

                        if(validStringToken)
                        {
                            //here, the constructor of the legoPiece is being called again !!! FIX
                            quarryStore->Add(tempLegoPiece); 
                        }
                        
                        //It's important that we do not delte the object right away after 
                        //appending, since we're passing the obejct to the append function
                        //by refernece and not by copy. So if we delete this object, 
                        //the linked list pointer will become pointing to nothing.
                    }

                }else if(buildingsFlag)
                {
                    buildQuantity = stoi(vec[1]);

                    //Adding the wanted quantity of the current building type, to the 
                    //integer holding the global amount of buildings.
                    requestedNumber += buildQuantity;

                    //loops for the amount of buildings in the current line in the 
                    //specification file.
                    for(int i = 0; i < buildQuantity; i++)
                    {
                        Building *tempBuilding;

                        //for every building we are trying to build, we are wrapping
                        //it into a try-catch block, since every building has the 
                        //possibility of not having enough lego pieces to be built.
                        try{
                            if(vec[0].find("1storeyhouse") != string::npos)
                            {
                                validStringToken = true;
                                tempBuilding = contractorBob->buildOneSH(quarryStore);
                                builtNumber++;
                            }
                            else if(vec[0].find("2storeyhouse") != string::npos)
                            {
                                validStringToken = true;
                                tempBuilding = contractorBob->buildTwoSH(quarryStore);
                                builtNumber++;
                            }
                            else if(vec[0].find("church") != string::npos)
                            {
                                validStringToken = true;
                                tempBuilding = contractorPeter->buildChurch(quarryStore);
                                builtNumber++;
                            }
                            else if(vec[0].find("hospital") != string::npos)
                            {
                                validStringToken = true;
                                tempBuilding = contractorJane->buildHospital(quarryStore);
                                builtNumber++;
                            }
                            else if(vec[0].find("university") != string::npos)
                            {
                                validStringToken = true;
                                tempBuilding = contractorJane->buildUniversity(quarryStore);
                                builtNumber++;
                            }else
                            {
                                cout << "   Specification number: " << specNum << " - invalid" << endl << endl;
                            }    
                            
                            if(validStringToken)
                            {
                                //this point if not reached when an exception is thrown while 
                                //trying to build the building. So we will only add to the city, 
                                //when the building process has not thrown any exceptions. 
                                cityStore->Add(tempBuilding);
                            }
                                
                        }catch(MissingLegoPiece& e) //when there was a missing lego piece
                        {
                            cout << "   Unsuccessful building of: " << vec[0] << " - not enough " << e.pieceType << "s." << endl;

                        }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
                        {
                            cout << "Exception: " << e.message << endl;

                        }catch(InvalidBuilding& e) //when the type of building does not exist. 
                        {
                            cout << "Exception: " << e.message << endl;
                        }
                        
                    }
                }
            }else
            {
                cout << "   Specification number: " << specNum << " - invalid" << endl << endl;
            }
        }

        delete contractorBob;
        delete contractorJane;
        delete contractorPeter;

    }
    catch (ifstream::failure &e)
    {
        cout << "Exception! " << e.what() << endl;
    }
    catch (...)
    {
        cout << "Exception!" << endl;
    }  

    percentage = ((float)builtNumber / (float)requestedNumber) * 100;

    cout << endl;
    cout << "------------------------------------" << endl;
    cout << "SUMMARY" << endl;
    cout << "------------------------------------" << endl;
    cout << "Number of requested buildings: " << requestedNumber << endl;
    cout << "Number of completed buildings: " << builtNumber << endl;
    cout << "Percentage completed: " << percentage << "%" << endl;

    cout << "------------------------------------" << endl;
    cout << "List of completed buildings:" << endl;
    cityStore->Print();
    cout << "------------------------------------" << endl;

    delete quarryStore;
    delete cityStore;


    /*
    TO ADD A BUILDING TO THE CITY: 

    Building *tempBuilding;
    tempBuilding = new OneSH;
    cityStore->Add(tempBuilding);
    */

    return 0;
}