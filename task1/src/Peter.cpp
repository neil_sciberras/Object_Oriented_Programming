#include "Peter.h"

using namespace std;

void Peter::petersMessage()
{
    cout << "I'm a contractor and I specialize in building churches!"<< endl;
}

Building* Peter::buildChurch(Quarry *quarry)
{
    try{
        return ChurchBuilder::buildChurch(quarry);

    }catch(MissingLegoPiece& e) //when there was a missing lego piece
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}