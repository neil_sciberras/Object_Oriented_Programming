#include "University.h"

using namespace std;

int University::UniCount = 0;

University::University()
{
    UniCount++;
    setID(UniCount);
}

University::~University()
{
    UniCount--;
}

int University::getCount()
{
    return UniCount;
}

void University::studentHouse()
{
    cout << "I am the students' house!" << endl;
}