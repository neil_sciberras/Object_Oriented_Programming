#include "Building.h"

int Building::count = 0;

Building::Building()
{
    count++;
}

Building::~Building()
{
    //when deleting a building, we need to decrement the count.
    count--;
}

int Building::getID()
{
    return ID;
}

void Building::setID(int pID)
{
    ID = pID;
}

int Building::getCount()
{
    return count;
}