#include "Brick.h"

using namespace std;

int Brick::BrickCount = 0;

int Brick::getCount()
{
    return BrickCount;
}

Brick::Brick()
{
    BrickCount++;
    setID(BrickCount);
}

Brick::~Brick()
{
    BrickCount--;
}

void Brick::reinforce()
{
    cout << "Bricks' function is to reinforce the structure of the building!" << endl;
}