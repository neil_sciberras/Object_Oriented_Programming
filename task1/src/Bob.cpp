#include "Bob.h"

using namespace std;

void Bob::bobsMessage()
{
    cout << "I'm a contractor and I specialize in building One and Two storey houses!"<< endl;
}

Building* Bob::buildOneSH(Quarry *quarry)
{
    try{
        return OneSHBuilder::buildOneSH(quarry);

    }catch(MissingLegoPiece& e) //when there was a missing lego piece
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}

Building* Bob::buildTwoSH(Quarry *quarry)
{
    try{
        return TwoSHBuilder::buildTwoSH(quarry);

    }catch(MissingLegoPiece& e) //when there was a missing lego piece
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }    
}