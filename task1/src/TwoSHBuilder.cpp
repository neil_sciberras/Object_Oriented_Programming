/*
LegoPieces needed:
    20 Bricks
    1 Door 
    3 Window
*/

#include "TwoSHBuilder.h"

using namespace std;

Building * TwoSHBuilder::buildTwoSH(Quarry *quarry)
{
    try{
        if(tryBuilding("2storeyhouse", quarry) == true)
        {
            Building * twoStoreyHouse = new TwoSH();
            return twoStoreyHouse;
        }

    }catch(MissingLegoPiece& e) //when the required legopiece is not found.
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}