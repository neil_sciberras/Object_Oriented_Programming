// LegoPiece.cpp

#include "LegoPiece.h"

//The reason you need this is because 'static int count' is a variable declaration, 
//but you need the definition in a single source file so that the linker knows what memory 
//location you're referring to when you use the name 'LegoPiece::count'
int LegoPiece::count = 0;

LegoPiece::LegoPiece()
{
    count++;
}

LegoPiece::~LegoPiece()
{
    count--;
}

int LegoPiece::getID()
{
    return ID;
}

void LegoPiece::setID(int pID)
{
    ID = pID;
}

int LegoPiece::getCount()
{
    return count;
}