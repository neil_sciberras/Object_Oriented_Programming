#include "Jane.h"

using namespace std;

void Jane::janesMessage()
{
    cout << "I'm a contractor and I specialize in building hospitals and universities!"<< endl;
}

Building* Jane::buildHospital(Quarry *quarry)
{
    try{
        return HospitalBuilder::buildHospital(quarry);

    }catch(MissingLegoPiece& e) //when there was a missing lego piece
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
    
}

Building* Jane::buildUniversity(Quarry *quarry)
{
    try{
        return UniversityBuilder::buildUniversity(quarry);

    }catch(MissingLegoPiece& e) //when there was a missing lego piece
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when a searched lego piece had a type that did not exist
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
    
}