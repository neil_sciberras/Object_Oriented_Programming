#include "TwoSH.h"

using namespace std;

int TwoSH::TwoSHCount = 0;

TwoSH::TwoSH()
{
    TwoSHCount++;
    setID(TwoSHCount);
}

TwoSH::~TwoSH()
{
    TwoSHCount--;
}

int TwoSH::getCount()
{
    return TwoSHCount;
}

void TwoSH::biggerFamily()
{
    cout << "I act as shelter to a normal sized family!" << endl;
}