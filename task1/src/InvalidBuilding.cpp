#include "InvalidBuilding.h"

InvalidBuilding::InvalidBuilding(string type)
{
    stringstream stream;
    stream << "'" << type << "' is an invalid building!";
    
    message = stream.str();
}