//Refer to http://www.dgp.toronto.edu/people/JamesStewart/270/9697f/notes/Nov25-tut.html

#include <iostream>
#include <typeinfo>
#include <string>

#include "LL.h"
#include "Building.h"
#include "LegoPiece.h"
#include "Door.h"
#include "Brick.h"
#include "Window.h"
#include "OneSH.h"
#include "TwoSH.h"
#include "Church.h"
#include "Hospital.h"
#include "University.h"

using namespace std;

template< class T >
LL<T>::LL()
{
    //setting both pointers to head and tail to null, since when the constructor is called, the 
    //list will not be created yet.
    head = NULL;
    tail = NULL;
}

template< class T >
void LL<T>::append(T * PointItem)
{
    //When appending, what we really need passed as argument is a pointer to the original 
    //item (pass by reference) and not a copy of its value. In this way, we retain all 
    //polymorphic information, which helps us in dynamic casting later and identifying what the 
    //object really represents. Example, from a LegoPiece object we could identify whether
    // it really is a Door, Window, etc. 

    LLnode< T > *temp = new LLnode< T >();

    //since temp is a pointer, we need '->'
    temp->data = *PointItem;
    temp->next = NULL; //since we're appending, there will be no other node behind this node, so 
    //the next pointer will be set to NULL.

    temp->setOrigPointer(PointItem);

    if(head == NULL)//if the list is empty (if the head is NULL, then the list is empty)
    {
        //when there is only one element in the list, it is both the head and the tail.
        head = temp;
        tail = temp;

        //temp is not going to be used anymore, so we set it equal to NULL
        temp = NULL;
    }else //if a list already exists
    {
        //When a list already exists, then we need to put the new node behind the tail node. 
        //To do this, we need to set the pinter of the old tail pointing to the new node. 
        tail->next = temp;

        //After the next pointer of the old tail is modified, then we set the set the new node as the tail. 
        tail = temp;
    }

}


template< class T >
void LL<T>::insertAt(T * PointItem, int index)
{
    //ASSUMING THAT THE LINKED LIST FIRST INDEX IS 0

    if(index > 0) //When inserting aftert the head
    {
        //The head and tail will be left untouched.
        
        //We will be inserting the new node between 2 consecutive ndoes, calling them Previous (at index-1) and Current (at index).
        
        //just creating the nodes.
        LLnode<T> *previous = new LLnode<T>();
        LLnode<T> *current = new LLnode<T>();

        //node representing the new node.
        LLnode<T>* newNode = new LLnode<T>();
        newNode->data = *PointItem;
        newNode->setOrigPointer(PointItem);

        //We start by setting the current node equal to the head, and then iterating until we meet the required index.
        current = head;

        for(int i = 0; i < index; i++)
        {
            //moving 1 node to the right.

            //the 'previous' becomes the 'current' node.
            previous = current;
            //'current' was pointing to the next node. 'current' needs to become equal to that, so 'current' is set equal to
            //the node it is pointing to.
            current = current->next;
        }
        
        //the previous will now be pointing to the new node.
        previous->next = newNode;	

        //and the new node will be pointing to the 'current' node.
        newNode->next = current;
    }else
    {
        //just creating the node
        LLnode<T>* newNode = new LLnode<T>();

        newNode->data = *PointItem;
        newNode->next = head;//the new node will be pointing to the node that was called the head before inserting the new node.
        newNode->setOrigPointer(PointItem);

        //the newNode will now be the new head.
        head = newNode;
    }
}

template< class T >
void LL<T>::deleteItem(int index)
{
    if(index == 0) //deleting the node currently serving as head.
    {
        //this 'temp' pointer points to the actual node serving as the current head. 
        LLnode<T>* temp = new LLnode<T>();
        temp = head;

        //setting the second node in the linked list as the new head.
        head=head->next;

        //deleting the node, by invoking delete, which calls the destructor of the class.
        delete temp->getOrigPointer();
        delete temp;

    }else if(index < (sizeOfList()-1)) //-1 because the first index is 0, so if the size is 10, the indices will be 0...9.
    {
        //using the same technique of when adding at a particular position.
        //using 2 reference points, the current node, and the one before it 'previous'.
        LLnode<T>* current = new LLnode<T>();
        LLnode<T>* previous = new LLnode<T>();

        //starting the iteration of nodes from the head.
        current=head;

        for(int i = 0; i < index; i++)
        {
            //moving to the next set of 'current and previous'
            previous = current;
            current = current->next;
        }

        //this will make 'previous' point to the node after the 'current' node, so skipping the 'current' node.
        previous->next = current->next;

        delete current->getOrigPointer();
        delete current; //calling the destructor to deallocate the memory occupied by that node.
    }
    else if(index == sizeOfList()-1) //deleting the last node
    {
        //Since we are deleting the last node, we need access to the 'second to last' node, to make it the new tail.
        //To access the second to last node, we need to iterate through the linked list, by using 2 nodes, 'current' and 
        //the one before it 'previous'.

        LLnode<T>* current = new LLnode<T>();
        LLnode<T>* previous = new LLnode<T>();

        //starting from the beginning.
        current = head;

        //loop until the node 'current' points to nothing, which means that the node 'current' is at the last node.
        while(current->next != NULL)
        {
            //moving previous and current one node forward.
            previous = current;
            current = current->next;	
        }
        
        //when current is at the last node, the new tail is the previous (second to last) node.
        tail = previous;
        //Also, previous (the new tail) will not be pointing to anything, since the last node will be deleted.
        previous->next = NULL;

        //calling the destructor to deallocate the necessary memory.
        delete current->getOrigPointer();
        delete current;
    }
}

template< class T >
int LL<T>::indexOf(T item)
{
    //Returns -1 on failure, and the index on success.

    //We are going to traverse the linked list, and compare the given item's ID with the ID 
    //of every other item in the linked list. 

    //this will temporarily hold each node.
    LLnode<T>* temp = new LLnode<T>();

    //the index will start from 0, so if the node is found immediately in the head, it will return index 0 as the idnex.
    int index = 0;

    //the first node that shoudl be checked is the head.
    temp = head;

    //this is set to false initially, and set to true when found.
    bool found = false;

    //keeps on looping until the node is found
    while(!found)
    {
        
        if(item.getID() == temp->data.getID())
        {
            found = true;
        }
        else if(temp->next != NULL) //if there is another node after this one set temp to the next node and inrement the index
        {
            temp = temp->next;
            index++;
        }else //if this was the last node, then break out of the loop with the found flag as false.
        {
            break;
        }
    }

    if(found) //when found (success) it returns the index of the node.
        return index;
    else //on failure, when the node was not found, a message is displayed and -1 is returned.
    {
        cout << "Item was not found." << endl;
        return -1;
    }
}

template< class T >
int LL<T>::sizeOfList()
{
    int count = 0;

    //temporary holder for the nodes, starting from the head.
    LLnode<T>* temp = new LLnode<T>();
    temp = head;

    //at some point temp will be pointing to NULL.
    //loop until the node is NULL.
    while (temp != NULL) 
    {
        count++;

        //setting temp equal to the next node.
        temp = temp->next; 
    }

    return count;
}

template< class T >
void LL<T>::print()
{
    //temporary storage of nodes.
    LLnode<T>* temp;// = new LLnode<T>();

    if(head == NULL)
    {
        cout << "Empty linked list" << endl;
    }
    else
    {

        //starting from the first node.
        temp = head;

        //When temp is NULL, meaning that it's trying to access the node after the last one, it will stop.
        while(temp != NULL)
        {
            //identifying what type of animal the current node has, and 
            //displaying the appropriate message.

            //Dynamic cast explanation found at: http://www.bogotobogo.com/cplusplus/dynamic_cast.php

            if(dynamic_cast<Door*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID() << " - Door" << endl;
            else if(dynamic_cast<Brick*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - Brick" << endl;
            else if(dynamic_cast<Window*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - Window" << endl;
            else if(dynamic_cast<OneSH*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - One Storey House" << endl;
            else if(dynamic_cast<TwoSH*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - Two Storey House" << endl;
            else if(dynamic_cast<Church*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID() << " - Church"<< endl;
            else if(dynamic_cast<Hospital*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - Hospital" << endl;
            else if(dynamic_cast<University*>(temp->getOrigPointer()) != NULL)
                cout << temp->data.getID()<< " - University" << endl;            
            else
                cout << "No dynamic_cast has matched!" << endl;            

            //accessing the next node.
            temp = temp->next;
        }
    }
}

template< class T >
T LL<T>::getNodeAt(int index)
{
    LLnode<T>* temp = new LLnode<T>();

    temp = head;

    for(int i = 0; i < index; i++)
    {
        temp = temp->next;
    }

    return temp->data;
}

template< class T >
int LL<T>::findPieceOfType(string type)
{   
    //NB. This function was only neede in the case of the linked list of legoPieces, so we its
    //functionality is only exceeded tot he legoPieces and not for the Buildings as well.

    //Returns -1 on failure, and returns the index of the found node on success.

    //We are going to traverse the linked list, and compare every item's type (door, brick,
    //or window) with the argument string. 

    //temporary storage of nodes.
    LLnode<T>* temp;

    //The index will start from 0, so if the node is found immediately in the head, 
    //it will return index 0 as the index.
    int index = 0;

    //Starting from the first node.
    temp = head;

    //When temp is NULL, meaning that it's trying to access the node after the last one,
    //it will stop.
    while(temp != NULL)
    {
        //Identifying what type of object the current node has, and displaying the 
        //appropriate message.

        //Dynamic cast explanation found at: http://www.bogotobogo.com/cplusplus/dynamic_cast.php

        if(dynamic_cast<Door*>(temp->getOrigPointer()) != NULL)
        {
            //find returns the position of the first character of the first match, else if
            //no matches are found, string::npos is returned.
            if(type.find("Door") != string::npos)
                return index;
        }
        
        if(dynamic_cast<Brick*>(temp->getOrigPointer()) != NULL)
        {
            if(type.find("Brick") != string::npos)
                return index;
        }
        
        if(dynamic_cast<Window*>(temp->getOrigPointer()) != NULL)
        {
            if(type.find("Window") != string::npos)
                return index;
        }      

        //accessing the next node.
        temp = temp->next;
        index++;
    }

    //If this point was reached, hence the while loop did not return, then none of the
    //items' type matched the string entered as argument. This means that there is none 
    //of these legoPieces. 
    
    //This will trigger the throwing of an exception in Quarry::FindPieceOfType
    return -1;
    
}



//We can't expect the compiler to compile all the above functions for every possibility of T, be it integer, float, etc.
//So what we do is tell the compiler which particular templates will be required, forcing it to compile them.
//As explained better in: https://stackoverflow.com/questions/8752837/undefined-reference-to-template-class-constructor

template class LL<LegoPiece>;
template class LL<Building>;