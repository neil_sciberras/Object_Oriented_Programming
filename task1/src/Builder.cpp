#include "Builder.h"
#include "OneSH.h"
#include "TwoSH.h"
#include "Church.h"
#include "Hospital.h"
#include "University.h"


Builder::Builder(string pName)
{
    this->name = pName;
}

string Builder::getName()
{
    return this->name;
}

//This function is protected. This means that the fucntion can only be accessed from the 
//derived classes. 
bool Builder::getPieces(string type, int amount, Quarry *quarry)
{
    //This function returns true when all the pieces are found in the quarry. 

    int index;
    int counter = 0;

    //Just deleting items from the linked list, as if we were using them to build something.
    for(int i = 0; i < amount; i++)
    {
        try 
        {
            //this line is the line that may throw the exception. 
            //If it throws the exception, then the execution of the try block stops
            //here, and the execution trnafers to the catch block.
            //But if it does not throw an exception, then the lines below it will
            //be executed. 
            index = quarry->FindPieceOfType(type);

            //from here onwards, we can assument that since FindPieceOfType did not throw 
            //any exception, then the piece was found and its index has been returned and 
            //stored in 'index'. Therefore we can return true and delete the piece from
            //the quarry.
            quarry->DeleteItemAtIndex(index);

            //When a lego piece is found, we increment the counter, which holds the amount
            //amount of pieces found.
            counter++;

        }catch(MissingLegoPiece& e) //when the required legopiece is not found.
        {
            //this re-thorws the exception tot he fucntion that called this one.
            throw;
        }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
        {
            //re-thorwing of the exception.
            throw;
        }
    }

    //Only when the amount of pieces found equals the amount required, we return true. 
    if(counter == amount)
        return true;
}


bool Builder::tryBuilding(string buildingType, Quarry *quarry)
{
    bool valid = false;
    //whether the building type is a valid one or not. 

    int doors, bricks, windows = 0;

    if(buildingType.find("1storeyhouse") != string::npos)
    {
        valid = true;

        OneSH tempObj;

        doors = tempObj.Doors;
        bricks = tempObj.Bricks;
        windows = tempObj.Windows;
    }
    else if(buildingType.find("2storeyhouse") != string::npos)
    {
        valid = true;

        TwoSH tempObj;

        doors = tempObj.Doors;
        bricks = tempObj.Bricks;
        windows = tempObj.Windows;
    }
    else if(buildingType.find("Church") != string::npos)
    {
        valid = true;
        
        Church tempObj;

        doors = tempObj.Doors;
        bricks = tempObj.Bricks;
        windows = tempObj.Windows;
    }
    else if(buildingType.find("Hospital") != string::npos)
    {
        valid = true;
        
        Hospital tempObj;

        doors = tempObj.Doors;
        bricks = tempObj.Bricks;
        windows = tempObj.Windows;
    }
    else if(buildingType.find("University") != string::npos)
    {
        valid = true;
        
        University tempObj;

        doors = tempObj.Doors;
        bricks = tempObj.Bricks;
        windows = tempObj.Windows;
    }
    
    //if the building type entered was a valid one
    if(valid)
    {
        bool windowsFound = false;
        bool doorsFound = false;
        bool bricksFound = false;

        try
        {
            //We start off by looking for the doors, since all the buildings, the least 
            //required piece (in terms of quantity) is the doors. So if there isn't enough
            //doors, we wouldnt have searched for a lot of pices for nothing, we would only
            //have searched for a small amount of pieces. 
            doorsFound = getPieces("Door", doors, quarry);
            
            if(doorsFound == true)
            {  
                windowsFound = getPieces("Window", windows, quarry);

                if(windowsFound == true)
                {
                    bricksFound = getPieces("Brick", bricks, quarry);
                }
            }

            return (doorsFound && windowsFound && bricksFound);

        }catch(MissingLegoPiece& e) //when the required legopiece is not found.
        {
            //this re-thorws the exception to the fucntion that called this one.
            throw;
        }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
        {
            //re-thorwing of the exception.
            throw;
        }
        
    } else
    {
        //throwing an exception that indicates that the building tyoe is invalid and does 
        //not exist.
        throw InvalidBuilding(buildingType);
    }
}