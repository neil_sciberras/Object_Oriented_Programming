/*
LegoPieces needed:
    60 Bricks
    10 Door 
    15 Window
*/

#include "HospitalBuilder.h"

using namespace std;

Building * HospitalBuilder::buildHospital(Quarry *quarry)
{
    try{
        if(tryBuilding("Hospital", quarry) == true)
        {
            Building * hospital = new Hospital();
            return hospital;
        }

    }catch(MissingLegoPiece& e) //when the required legopiece is not found.
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}