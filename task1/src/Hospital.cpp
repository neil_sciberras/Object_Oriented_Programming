#include "Hospital.h"

using namespace std;

int Hospital::HospitalCount = 0;

Hospital::Hospital()
{
    HospitalCount++;
    setID(HospitalCount);
}

Hospital::~Hospital()
{
    HospitalCount--;
}

int Hospital::getCount()
{
    return HospitalCount;
}

void Hospital::sickHouse()
{
    cout << "I am the sicks' house!" << endl;
}