#include "Window.h"

using namespace std;

int Window::WindowCount = 0;

int Window::getCount()
{
    return WindowCount;
}

Window::Window()
{
    WindowCount++;
    setID(WindowCount);
}

Window::~Window()
{
    WindowCount--;
}

void Window::aperture()
{
    cout << "Airflow can now pass!" << endl;
}