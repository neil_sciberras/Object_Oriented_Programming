#include "OneSH.h"

using namespace std;

//Initialization of static variables.
int OneSH::OneSHCount = 0;

const int OneSH::Bricks = 10;
const int OneSH::Doors = 1;
const int OneSH::Windows = 1;

OneSH::OneSH()
{
    OneSHCount++;
    setID(OneSHCount);
}

OneSH::~OneSH()
{
    OneSHCount--;
}

int OneSH::getCount()
{
    return OneSHCount;
}

void OneSH::smallFamily()
{
    cout << "I act as shelter to a small family!" << endl;
}