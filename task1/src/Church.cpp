#include "Church.h"

using namespace std;

int Church::ChurchCount = 0;

Church::Church()
{
    ChurchCount++;
    setID(ChurchCount);
}

Church::~Church()
{
    ChurchCount--;
}

int Church::getCount()
{
    return ChurchCount;
}

void Church::godsHouse()
{
    cout << "I am God's House!!" << endl;
}