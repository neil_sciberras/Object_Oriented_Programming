//On how the unique identifiers work: 
//  https://books.google.com.mt/books?id=6tjfmnKhT24C&pg=PT316&lpg=PT316&dq=id+of+class+object+increments+with+every+object+in+cpp&source=bl&ots=ODyFMGQJAd&sig=V-_nn2NfsKtKQp1nGeBvntMpOE4&hl=en&sa=X&ved=0ahUKEwjo5ZONt57YAhVKbhQKHV7nCwcQ6AEIYDAI#v=onepage&q=id%20of%20class%20object%20increments%20with%20every%20object%20in%20cpp&f=false

#include "Door.h"

using namespace std;

int Door::DoorCount = 0;

int Door::getCount()
{
    return DoorCount;
}

Door::Door()
{
    DoorCount++;
    setID(DoorCount);
}

Door::~Door()
{
    DoorCount--;
}

void Door::openDoor()
{
    cout << "People can enter/exit!" << endl;
}