//All the functions needed for this are already defined in the LL class, so we just call them. 

#include "City.h"

void City::Add(Building * building)
{
    list.append(building);
}

void City::DeleteItem(Building * building)
{
    int index = list.indexOf(*building);

    //indexOf returns -1 on failure
    if(index == -1)
        cout << "Building not found in City" << endl;
    else
    {
        //this remove the building from the city
        list.deleteItem(index);

        //deletes the pointer, and deallocates the memory block pointed to by building.
        delete building;
    }
}

void City::DeleteItemAtIndex(int index)
{
    list.deleteItem(index);
}

void City::Print()
{
    list.print();
}

int City::FindPieceOfType(string buildingType)
{
    bool valid = false;
    int index;

    //string::find() finds the first appearance of "Door" in 'buildingType'
    //and returns the position it was found. If it is not fund anywhere, then 
    //string::npos is returned.

    if(buildingType.find("1storeyhouse") != string::npos)
        valid = true;
    if(buildingType.find("2storeyhouse") != string::npos)
        valid = true;
    if(buildingType.find("church") != string::npos)
        valid = true;
    if(buildingType.find("hospital") != string::npos)
        valid = true;
    if(buildingType.find("university") != string::npos)
        valid = true;

    if(valid)
    {
        index = list.findPieceOfType(buildingType);

        //doube checking that an item was actually found.
        if(index != -1)
            return index;
        else 
            cout << "EXCEPTION" << endl;
    }
    else 
    {
        return -1;
    }
    
}