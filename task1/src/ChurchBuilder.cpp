/*
LegoPieces needed:
    40 Bricks
    3 Door 
    10 Window
*/

#include "ChurchBuilder.h"

using namespace std;

Building * ChurchBuilder::buildChurch(Quarry *quarry)
{
    try{
        if(tryBuilding("Church", quarry) == true)
        {
            Building * church = new Church();
            return church;
        }

    }catch(MissingLegoPiece& e) //when the required legopiece is not found.
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}