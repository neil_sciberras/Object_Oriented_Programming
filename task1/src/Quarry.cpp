//All the functions needed for this are already defined in the LL class, so we just call them. 

#include "Quarry.h"

void Quarry::Add(LegoPiece * piece)
{
    list.append(piece);
}

void Quarry::DeleteItem(LegoPiece * piece)
{
    int index = list.indexOf(*piece);

    //indexOf returns -1 on failure
    if(index == -1)
        cout << "LegoPiece not found in Quarry" << endl;
    else
    {
        //this remove the legoPiece from the quarry
        list.deleteItem(index);

        //deletes the pointer, and deallocates the memory block pointed to by piece.
        delete piece;
    }
}

void Quarry::DeleteItemAtIndex(int index)
{
    list.deleteItem(index);
}

void Quarry::Print()
{
    list.print();
}

int Quarry::FindPieceOfType(string type)
{
    //Indicates the vaidity of the 'type' of the lego piece. 
    bool valid = false;
    //Holds the index of the lego piece in the quarry. 
    int index;

    if(type.find("Door") != string::npos)
        valid = true;
    else if(type.find("Brick") != string::npos)
        valid = true;
    else if(type.find("Window") != string::npos)
        valid = true;

    if(valid)
    {
        index = list.findPieceOfType(type);

        //When findPieceOfType returns -1, it means that the lego piece was not found,
        //so we throw this exception. 
        if(index == -1)
        {
            throw MissingLegoPiece(type);
        }else
        {
            return index;
        }
    }
    else 
    {
        //When th valid flag is still false, meaning that the 'type' argument entered was
        //not one of Door, Brick, or Window, then it is invalid. Hence we throw this 
        //exception. 
        throw InvalidPiece(type);
    }
    
}

int Quarry::size()
{
    return list.sizeOfList();
}