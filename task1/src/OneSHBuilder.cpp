/*
LegoPieces needed:
    10 Bricks
    1 Door 
    1 Window
*/

#include "OneSHBuilder.h"

using namespace std;

Building * OneSHBuilder::buildOneSH(Quarry *quarry)
{
    try{
        if(tryBuilding("1storeyhouse", quarry) == true)
        {
            Building * oneStoreyHouse = new OneSH();
            return oneStoreyHouse;
        }

    }catch(MissingLegoPiece& e) //when the required legopiece is not found.
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}