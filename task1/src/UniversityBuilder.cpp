/*
LegoPieces needed:
    80 Bricks
    10 Door 
    20 Window
*/

#include "UniversityBuilder.h"

using namespace std;

Building * UniversityBuilder::buildUniversity(Quarry *quarry)
{
    try{
        if(tryBuilding("University", quarry) == true)
        {
            Building * university = new University();
            return university;
        }

    }catch(MissingLegoPiece& e) //when the required legopiece is not found.
    {
        //this re-thorws the exception to the fucntion that called this one.
        throw;
    }catch(InvalidPiece& e) //when the type of lego piece does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }catch(InvalidBuilding& e) //when the type of building does not exist. 
    {
        //re-thorwing of the exception.
        throw;
    }
}