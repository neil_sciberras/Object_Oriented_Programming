// csv_reader.cpp

#include <sstream>
#include "csv_reader.h"

#define WHITE_SPACE " \t"

string trimLeft(string &s);
string trimRight(string &s);
string trim(string &s);

CsvReader::CsvReader(string filename, string delimiter, bool ignoreComments)
{
    //inputStream is the input stream. The file is open and stored in theis input stream.      
    inputStream.open(filename);

    //good() Returns true if the the most recent I/O operation on the stream completed successfully.
    if (!inputStream.good())
    {
        //outputStringStream is an output string stream 
        ostringstream outputStringStream;
        outputStringStream << "Error opening file \"" << filename << "\"";

        //outputStringStream.str() turns the string stream into a normal string
        throw ifstream::failure(outputStringStream.str());
    }

    //Remember, 'this' is a constant pointer that holds the memory address of the current object. 
    this->delimiter = delimiter;
    this->ignoreComments = ignoreComments;
}

CsvReader::~CsvReader()
{
    //The destructor just closes the input stream.
    inputStream.close();
}

bool CsvReader::readLine(vector<string> &vec)
{
    //vec is a reference to a vector of strings. With references we do not need to de-reference, unlike pointers.

    //'clear()' removes all elements from the vector.
    vec.clear();
    string line;

    // Get the next line.

    for (;;)
    {
        //end of file
    	if (inputStream.eof())
    		return false; //returning false will exit the whole method, because the method has a return type of bool.

        //Extracts characters from 'inputStream' and stores them into 'line' until the newline character is found. 
    	getline(inputStream, line);

        // Abort if no data could be read and the EOF has not been reached.
        if (inputStream.fail() && !inputStream.eof())
            throw ifstream::failure("Error reading from file");

        //Removes white spaces from both ends of the line. 
        line = trim(line);

        // Ignore empty line.
        if (line.length() == 0)
            continue;

        if (ignoreComments)
        {
            // Ignore comment.
            //Comments in the csv input file begin with the character '#'.
            if (line[0] == '#')
                continue;
        }

        break;
    }

    // Extract tokens from the line and populate the passed-in vector.

    string token;
    size_t pos;

    for (;;)
    {
        pos = line.find(delimiter);
    
        //If the end of string has not been reached yet. 
        if (pos != string::npos) // delimiter found
        {
            //The part of the line which makes up the token, is stored in a string variable.
            token = line.substr(0, pos);

            //removes the token from the line.
            line.erase(0, pos + delimiter.length());

            //removes any whitespace that the token may contain.
            token = trim(token);

            //stores the token in the vector.
            vec.push_back(token);
        }
        else
        {
            //when no matches are found, 'find' returns string::npos. 
            //When the line only contains the last token available, then we remove any whitespace it contains using the trim.
            token = trim(line);

            //we add the token to the vector. 
            vec.push_back(token);
            break;
        }
    }

	return true;
}

string trimLeft(string &s)
{
    //position of first character that is not a white space. 
    size_t pos = s.find_first_not_of(WHITE_SPACE);

    //npos means the end of the string. 
    //If we arrived at the end of the string.
    if (pos == string::npos)
        return "";

    //Returns the string constructed of the part of s; from the current position onwards. 
    //Removes the part of the string on the left of the current position. 
    return s.substr(pos, string::npos);
}

string trimRight(string &s)
{
    //position of last character that is not a white space. 
    size_t pos = s.find_last_not_of(WHITE_SPACE);

    //npos means the end of the string. 
    //If we arrived at the end of the string.
    if (pos == string::npos)
        return "";

    //Similar to trimLeft, but returns the first part of the string, until the current position is reached,
    //and disposes of the part on the right. 
    return s.substr(0, pos + 1);
}

string trim(string &s)
{
    //Returns the line without the whitespaces at both ends.

    //Disposes of the part on the left of the token. 
    string tmpStr = trimLeft(s);

    //Disposes of the part tot he right of the token. 
    return trimRight(tmpStr);
}
