'compile.sh' and 'run.sh' are bash files to compile and run the program. 

input1.csv is a file with a set of normal specifications that will successfully build some buildings. 

input2.csv is a test file, which tests the program's ability to detect invalid specifications.

(Change the command line argument in the bash file 'run.sh' to input these files to the program) 
