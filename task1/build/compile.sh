#! /bin/sh

g++ -Wno-reorder -std=c++11 -o main Launcher.cpp Brick.cpp Builder.cpp Building.cpp Church.cpp ChurchBuilder.cpp City.cpp csv_reader.cpp Door.cpp Hospital.cpp HospitalBuilder.cpp LegoPiece.cpp  LL.cpp OneSH.cpp OneSHBuilder.cpp Quarry.cpp TwoSH.cpp TwoSHBuilder.cpp University.cpp UniversityBuilder.cpp Window.cpp Bob.cpp Jane.cpp Peter.cpp MissingLegoPiece.cpp InvalidPiece.cpp InvalidBuilding.cpp
# -Wno-reorder, this suppresses the warnings about the order of initialization of objects,
# like "will be initialized after.."