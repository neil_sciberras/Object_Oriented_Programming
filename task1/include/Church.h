#ifndef CHURCH
#define CHURCH

#include "Building.h"

class Church:public Building
{
    private:
        static int ChurchCount;
    public:
        static const int Bricks = 40;
        static const int Doors = 3;
        static const int Windows = 10;
        Church();
        ~Church();
        int getCount();
        void godsHouse();        
};

#endif