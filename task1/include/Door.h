#ifndef DOOR
#define DOOR

#include "LegoPiece.h"

class Door:public LegoPiece
{
    private:
        static int DoorCount;
    public:
        Door();
        ~Door();
        int getCount();
        void openDoor();
};

#endif