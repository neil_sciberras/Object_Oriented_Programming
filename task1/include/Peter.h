#ifndef PETER
#define PETER

#include "ChurchBuilder.h"

using namespace std;

class Peter: public ChurchBuilder
{
    public:
        Peter():ChurchBuilder("Peter"), Builder("Peter"){}
        void petersMessage();
        Building *buildChurch(Quarry *quarry);

};

#endif