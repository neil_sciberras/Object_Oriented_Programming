#ifndef HOSPITALBUILDER
#define HOSPITALBUILDER

#include "Builder.h"
#include "Hospital.h"

using namespace std;

class HospitalBuilder: virtual public Builder
{
    //All the member functions are left intact, and the implementations are left like the 
    //ones in the super class Builder. 

    //What the constructor of HospitalBuilder does is that it just calls the constructor
    //of its base class, Builder, and passes the argument to it. 
    public:
        HospitalBuilder(string name):Builder(name){}
        virtual Building *buildHospital(Quarry *quarry);

};

#endif 