#ifndef TWOSHBUILDER
#define TWOSHBUILDER

#include "Builder.h"
#include "TwoSH.h"

using namespace std;

class TwoSHBuilder: virtual public Builder
{
    //All the member functions are left intact, and the implementations are left like the 
    //ones in the super class Builder. 

    public:
        TwoSHBuilder(string name):Builder(name){}
        virtual Building *buildTwoSH(Quarry *quarry);

};

#endif