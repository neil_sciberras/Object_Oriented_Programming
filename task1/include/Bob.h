#ifndef BOB
#define BOB

#include "OneSHBuilder.h"
#include "TwoSHBuilder.h"

using namespace std;

class Bob: public OneSHBuilder, public TwoSHBuilder
{
    public:
        //Since, the parent classes of this class (OneSHBuilder and TwoSHBuider) are 
        //inheriting in vrtual mode, it is assured that only one copy of the base class is
        //created, although the constructor of the base class may be called multiple 
        //times.
        Bob():OneSHBuilder("Bob"), TwoSHBuilder("Bob"), Builder("Bob"){}
        void bobsMessage();
        Building *buildOneSH(Quarry *quarry);
        Building *buildTwoSH(Quarry *quarry);

};

#endif