#ifndef TWOSTOREY
#define TWOSTOREY

#include "Building.h"

class TwoSH:public Building
{
    private:
        static int TwoSHCount;
    public:
        static const int Bricks = 20;
        static const int Doors = 1;
        static const int Windows = 3;
        TwoSH();
        ~TwoSH();
        int getCount();
        void biggerFamily();        
};

#endif