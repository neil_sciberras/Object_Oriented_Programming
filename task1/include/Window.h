#ifndef WINDOW
#define WINDOW

#include "LegoPiece.h"

class Window:public LegoPiece
{
    private:
        static int WindowCount;
    public:
        Window();
        ~Window();
        int getCount();
        void aperture();
};

#endif