//LegoPiece.h

#ifndef LEGOPIECE
#define LEGOPIECE

#include <iostream>

using namespace std;

class LegoPiece
{
    private:
        static int count;
    	int ID;

    public:
        LegoPiece();
        virtual ~LegoPiece(); //set to virtual, so the delete can be called on polymorphic objects of type LegoPiece.
        int getID();
        void setID(int pID);
        virtual int getCount();
};


#endif