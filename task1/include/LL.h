 //Linked List declarations, using templates.

#ifndef LINKEDLIST
#define LINKEDLIST

#include "LLnode.h"

using namespace std;

template<class T>

class LL
{
    private:
        //These are pointers to nodes, where each node is of type
        //LLnode<T>. 
        LLnode<T> * head; //pointer to the first element 
        LLnode<T> * tail; //pointer to the last element

    public:
        LL(); //constructor
        ~LL() {}; //destructor
        void append(T * PointItem); //appending a node of type T to the linked list
        void insertAt(T * PointItem, int index); //b
        void deleteItem(int index); //c
        int indexOf(T item); //d
        int sizeOfList(); //e
        void print(); //f
        T getNodeAt(int index);
        int findPieceOfType(string type);


};

#endif