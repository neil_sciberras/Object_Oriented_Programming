
#ifndef ONESHBUILDER
#define ONESHBUILDER

#include "Builder.h"
#include "OneSH.h"

using namespace std;

class OneSHBuilder: virtual public Builder
{
    //All the member functions are left intact, and the implementations are left like the 
    //ones in the super class Builder. 

    public:
        OneSHBuilder(string name):Builder(name){}
        virtual Building *buildOneSH(Quarry *quarry);

};

#endif