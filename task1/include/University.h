#ifndef UNI
#define UNI

#include "Building.h"

class University:public Building
{
    private:
        static int UniCount;
    public:
        static const int Bricks = 80;
        static const int Doors = 10;
        static const int Windows = 20;
        University();
        ~University();
        int getCount();
        void studentHouse();        
};

#endif