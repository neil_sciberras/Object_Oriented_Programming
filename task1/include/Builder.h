/*
Each seperate builder class will inherit and implement this abstract class.
*/

#ifndef BUILDER
#define BUILDER

#include <string>
#include "Quarry.h"
#include "Building.h"

using namespace std;

class Builder
{
    private:
        string name;
    
    public:
        Builder(string pName);
        //getName CAN be overriden, but can be left be left the same aswell in the classes
        //that inherit from this one.
        string getName();
        //These will be implemented in the derived classes. 
        virtual Building *buildOneSH(Quarry *quarry) {}
        virtual Building *buildTwoSH(Quarry *quarry) {}
        virtual Building *buildChruch(Quarry *quarry) {}
        virtual Building *buildHospital(Quarry *quarry) {}
        virtual Building *buildUniversity(Quarry *quarry){}
    
    protected:
        bool getPieces(string type, int amount, Quarry *quarry);
        bool tryBuilding(string buildingType, Quarry *quarry);

};

#endif



/*
To keep in mind when using multiple inheritance, to avoid the diamond problem:
This will be the topmost class that the 5 builders will inherit from (the 5 builders 
are the OneSHBuilder, TwoSHBuilder, ChurchBuilder, HospitalBuilder, UniversityBuilder).
These 5 builders should inheirt in 'virtual public' way. If they inherit in 'public' only,
then 5 copies of this top most class (Builder) will be created, and then in the bottom 
most class (the contractors) there will be ambiguity, because there will be several versions 
of the methods offered by this class. When they inherit in virtual publc, only one copy
of this class is created, and hence no ambiguity is created.

https://www.geeksforgeeks.org/multiple-inheritance-in-c/
*/