#ifndef QUARRY
#define QUARRY

#include <iostream>

#include "LL.h"
#include "LegoPiece.h"
#include "MissingLegoPiece.h"
#include "InvalidPiece.h"
#include "InvalidBuilding.h"

class Quarry
{
    private:
        LL<LegoPiece> list;

    public:
        void Add(LegoPiece * piece);
        void DeleteItem(LegoPiece * piece);
        void DeleteItemAtIndex(int index);
        void Print();
        int FindPieceOfType(string type);
        int size();
};

#endif