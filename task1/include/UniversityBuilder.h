#ifndef UNIBUILDER
#define UNIBUILDER

#include "Builder.h"
#include "University.h"

using namespace std;

class UniversityBuilder: virtual public Builder
{
    //All the member functions are left intact, and the implementations are left like the 
    //ones in the super class Builder. 

    public:
        UniversityBuilder(string name):Builder(name){}
        virtual Building *buildUniversity(Quarry *quarry);

};

#endif