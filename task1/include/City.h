#ifndef CITY
#define CITY

#include <iostream>

#include "LL.h"
#include "Building.h"

class City
{
    private:
        LL<Building> list;

    public:
        void Add(Building * building);
        void DeleteItem(Building * building);
        void DeleteItemAtIndex(int index);
        void Print();
        int FindPieceOfType(string buildingType);
};

#endif