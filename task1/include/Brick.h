#ifndef BRICK
#define BRICK

#include "LegoPiece.h"

class Brick:public LegoPiece
{
    private:
        static int BrickCount;
    public:
        Brick();
        ~Brick();
        int getCount();
        void reinforce();
};

#endif