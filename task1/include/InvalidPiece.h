//This is a class whose instances will be thrown as exceptions. 

#ifndef INVALID
#define INVALID

#include <iostream>
#include <sstream>

using namespace std;

class InvalidPiece: public exception
{
    public:
        string message;
        InvalidPiece(string type);        
};

#endif