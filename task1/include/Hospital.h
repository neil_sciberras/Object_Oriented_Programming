#ifndef HOSPITAL
#define HOSPITAL

#include "Building.h"

class Hospital:public Building
{
    private:
        static int HospitalCount;
    public:
        static const int Bricks = 60;
        static const int Doors = 10;
        static const int Windows = 15;
        Hospital();
        ~Hospital();
        int getCount();
        void sickHouse();        
};

#endif