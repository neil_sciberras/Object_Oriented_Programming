//This is a class whose instances will be thrown as exceptions. 

#ifndef INVALIDBUILDING
#define INVALIDBUILDING

#include <iostream>
#include <sstream>

using namespace std;

class InvalidBuilding: public exception
{
    public:
        string message;
        InvalidBuilding(string type);        
};

#endif