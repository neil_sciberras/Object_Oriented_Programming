//The node structure of the linked list, using templates so as to be 
//generic.
#include <iostream>
using namespace std;
template <class T>
class LLnode 
{
    private:
        T * originalPointer; 
        //This is the pointer to the original data item.
        //This was done so as the dynamic cast in LL::print works fine.
        //The dynamic cast needs the original pointer to work, and not a copy of the data. 
    public:
        T data;
        LLnode<T> * next;
        T * getOrigPointer();
        void setOrigPointer(T * pointer);
       
};

//getter function since the origPointer is a private member.
template <class T>
T * LLnode<T>::getOrigPointer()
{
    return originalPointer;
}

//setter function since the origPointer is a private member.
template <class T>
void LLnode<T>::setOrigPointer(T * pointer)
{
    originalPointer = pointer;
}