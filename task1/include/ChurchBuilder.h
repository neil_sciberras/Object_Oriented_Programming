#ifndef CHURCHBUILDER
#define CHURCHBUILDER

#include "Builder.h"
#include "Church.h"

using namespace std;

class ChurchBuilder: virtual public Builder
{
    //All the member functions are left intact, and the implementations are left like the 
    //ones in the super class Builder. 

    public:
        ChurchBuilder(string name):Builder(name){}
        virtual Building *buildChurch(Quarry *quarry);

};

#endif