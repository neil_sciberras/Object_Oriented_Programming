#ifndef BUILDING
#define BUILDING

#include <iostream>

using namespace std;

class Building
{
    private:
        static int count;
    	int ID;

    public:
        Building();
        virtual ~Building(); //set to virtual, so the delete can be called on polymorphic objects of type Building.
        int getID();
        void setID(int pID);
        virtual int getCount();
};


#endif