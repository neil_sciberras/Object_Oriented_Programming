#ifndef ONESTOREY
#define ONESTOREY

#include "Building.h"

class OneSH:public Building
{
    private:
        static int OneSHCount; //these are only the declarations of the sttaic variables and not initialization.
    public:
        static const int Bricks;
        static const int Doors;
        static const int Windows;
        OneSH();
        ~OneSH();
        int getCount();
        void smallFamily();        
};

#endif