//This is a class whose instances will be thrown as exceptions. 

#ifndef MISSING
#define MISSING

#include <iostream>
#include <sstream>

using namespace std;

class MissingLegoPiece: public exception
{
    public:
        string pieceType;
        MissingLegoPiece(string type);        
};

#endif