#ifndef JANE
#define JANE

#include "HospitalBuilder.h"
#include "UniversityBuilder.h"

using namespace std;

class Jane: public HospitalBuilder, public UniversityBuilder
{
    public:
        Jane():HospitalBuilder("Jane"), UniversityBuilder("Jane"), Builder("Jane"){}
        void janesMessage();
        Building *buildHospital(Quarry *quarry);
        Building *buildUniversity(Quarry *quarry);

};

#endif